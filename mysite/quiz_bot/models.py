from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
#use score function in models

# Create your models here.


class DsAnswer(models.Model):
	question = models.TextField(max_length=1000)
	correct_answer = models.TextField(max_length=1000)
	created_at= models.DateTimeField(default = timezone.now)

	def __str__(self):
		return self.question

class UserAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	question = models.ForeignKey(DsAnswer,on_delete=models.CASCADE)
	user_answer = models.TextField(max_length=1000)
	created_at = models.DateTimeField(default = timezone.now)
	score = models.FloatField()

	def __str__(self):
		return self.user_answer


		

