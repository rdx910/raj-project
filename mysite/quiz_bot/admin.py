from django.contrib import admin
from .models import DsAnswer,UserAnswer
# Register your models here.

admin.site.register(DsAnswer)
admin.site.register(UserAnswer)